import { get } from "https";

// Make our https request and return a string representation of the response body
export function makeGetRequest(url) {
    return new Promise((resolve, reject) => {
        get(url, response => {
            let body = "";

            response.on("end", () => {
                resolve(body.toString());
            });

            response.on("data", chunk => {
                body += chunk;
            })

            response.on("error", error => {
                reject(error);
            });
        });
    });
}

// Filter out any strings that cannot be represented as Numbers
// also must handle falsy cases like 0 and an empty string 
export function numberOrZero(input) {
    return (Number(input) || (Number(input) === 0)) && input !== "";
};

