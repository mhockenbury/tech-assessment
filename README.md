# tech-assessment

## Dependencies:
Node v14

## Make it go (aka Run the app)
```
# clone the repo
git clone https://gitlab.com/mhockenbury/tech-assessment.git     
# Run the app
npm run start
```
## Tech assessment:
Write a Javascript application that gets data from the following URL and returns the lowest three integers with no duplicates.

https://www.iwillfearnoevil.com/screen/string.txt