import { makeGetRequest, numberOrZero } from "./utils.js";

async function main() {
    // make an HTTPS get request and parse the newline delimited text body into an array of strings
    const response = await makeGetRequest("https://www.iwillfearnoevil.com/screen/string.txt");
    const arrayResponse = response.split("\n");

    // 1. Filter out only Numbers
    const onlyNumbers = arrayResponse.filter(numberOrZero)
    // 2. use Set to dedupe array of numbers
    const uniqueNumbers = Array.from(new Set(onlyNumbers))
    // 3. Sort the array of numbers (ascending order)
    const sortedNumbers = (uniqueNumbers.sort((a,b) => a - b))
    // 4. Return the first 3 elements of the array (in this case the lowest 3 numbers due to ascending ordering) 
    const answer = sortedNumbers.slice(0,3);

    console.log("Lowest 3 integers are: ", answer.join(", "));
    process.exit(0);
}

main();